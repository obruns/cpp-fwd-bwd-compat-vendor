#include "vendor.hpp"
#include "vendor_impl.hpp"

#include <cstdio>
#include <cstdlib>
#include <memory>

#include <dlfcn.h>

auto main(int argc, char *argv[]) -> int {
  if (argc < 2) {
    std::fprintf(stderr, "usage: %s path/to/libclient.so\n", argv[0]);
    return EXIT_FAILURE;
  }

  auto lib_descriptor = [&]() {
    auto deleter = [](auto handle) { dlclose(handle); };
    return std::unique_ptr<void, decltype(deleter)>{dlopen(argv[1], RTLD_NOW),
                                                    deleter};
  }();
  if (lib_descriptor == nullptr) {
    std::fprintf(stderr, "%s\n", dlerror());
    return EXIT_FAILURE;
  }

  auto client_implementation =
      (client_fun_type)dlsym(lib_descriptor.get(), mangled_fn_name);
  if (client_implementation == nullptr) {
    std::fprintf(stderr, "%s\n", dlerror());
    return EXIT_FAILURE;
  }

  call_client(client_implementation);

  return EXIT_SUCCESS;
}
