#pragma once

#include <cstdint>

struct type_passed_to_the_client {
  std::uint32_t one;
  std::uint32_t two;
};

auto to_be_implemented_by_the_client(const type_passed_to_the_client &input)
    -> void;

using client_fun_type = void (*)(const type_passed_to_the_client &);

constexpr auto mangled_fn_name{
    "_Z31to_be_implemented_by_the_clientRK25type_passed_to_the_client"};
