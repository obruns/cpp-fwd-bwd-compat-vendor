# TODO This file has been adapted from an existing somelibConfig.cmake file
# and may not be optimal. See the following resources
#
# * https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html#creating-packages
# * https://cmake.org/cmake/help/latest/manual/cmake-packages.7.html#creating-relocatable-packages
# * https://cmake.org/cmake/help/latest/command/file.html#generate
# * https://cmake.org/cmake/help/latest/manual/cmake-buildsystem.7.html#transitive-usage-requirements
# * https://cmake.org/cmake/help/latest/guide/using-dependencies/index.html

set(VENDOR_MAJOR_VERSION "1")
set(VENDOR_MINOR_VERSION "0")
set(VENDOR_BUILD_VERSION "0")

# The libraries.
set(VENDOR_LIBRARIES "vendor")

# The configuration options.
set(VENDOR_BUILD_SHARED_LIBS "TRUE")

get_filename_component(SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
if(EXISTS ${SELF_DIR}/vendorTargets.cmake)
  # This is an install tree
  include(${SELF_DIR}/vendorTargets.cmake)
endif()

# Backward compatible part:
set(VENDOR_FOUND       TRUE)
